<?php
// $Id $

/**
 * @file
 * Webform module phone component.
 */

/**
 * Implementation of _webform_defaults_component().
 */
function _webform_defaults_date_monthyear() {
  return array(
    'name' => '',
    'form_key' => NULL,
    'pid' => 0,
    'weight' => 0,
    'value' => '',
    'mandatory' => 0,
    'extra' => array(
      'width' => '',
      'unique' => 0,
      'disabled' => 0,
      'title_display' => 0,
      'description' => '',
      'attributes' => array(),
    ),
  );
}

/**
 * Implementation of _webform_theme_component().
 */
function _webform_theme_date_monthyear() {
  return array(
    'webform_display_date_monthyear' => array(
      'arguments' => array('component' => NULL, 'value' => NULL, 'format' => 'plain'),
    ),
  );
}

/**
 * Implementation of _webform_edit_component().
 */
function _webform_edit_date_monthyear($component) {
  $form['value'] = array(
    '#type' => 'textfield',
    '#title' => t('Default value'),
    '#default_value' => $component['value'],
    '#description' => t('The default value of the field.') . theme('webform_token_help'),
    '#size' => 60,
    '#maxlength' => 127,
    '#weight' => 0,
    '#attributes' => ($component['value'] == '' && count(form_get_errors()) == 0) ? array('disabled' => TRUE) : array(),
    '#id' => 'monthyear-value',
  );
  $form['display']['width'] = array(
    '#type' => 'textfield',
    '#title' => t('Width'),
    '#default_value' => $component['extra']['width'],
    '#description' => t('Width of the textfield.') . ' ' . t('Leaving blank will use the default size.'),
    '#size' => 10,
    '#maxlength' => 15,
    '#parents' => array('extra', 'width'),
  );
  $form['display']['disabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Disabled'),
    '#return_value' => 1,
    '#description' => t('Make this field non-editable. Useful for setting an unchangeable default value.'),
    '#weight' => 11,
    '#default_value' => $component['extra']['disabled'],
    '#parents' => array('extra', 'disabled'),
  );
  $form['validation']['unique'] = array(
    '#type' => 'checkbox',
    '#title' => t('Unique'),
    '#return_value' => 1,
    '#description' => t('Check that all entered values for this field are unique. The same value is not allowed to be used twice.'),
    '#weight' => 1,
    '#default_value' => $component['extra']['unique'],
    '#parents' => array('extra', 'unique'),
  );
  return $form;
}

/**
 * Element validation function for the monthyear edit form.
 */
function _webform_edit_date_monthyear_validate($element, &$form_state) {
  if ($form_state['values']['user_phone']) {
    $form_state['values']['value'] = '%userphone';
  }
}

/**
 * Implementation of _webform_render_component().
 */
function _webform_render_date_monthyear($component, $value = NULL, $filter = TRUE) {
  global $user;
  $element = array(
    '#type' => 'textfield',
    '#title' => $filter ? _webform_filter_xss($component['name']) : $component['name'],
    '#title_display' => $component['extra']['title_display'] ? $component['extra']['title_display'] : 'before',
    '#default_value' => _webform_filter_values($component['value']),
    '#required' => $component['mandatory'],
    '#weight' => $component['weight'],
    '#description' => $filter ? _webform_filter_descriptions($component['extra']['description']) : $component['extra']['description'],
    '#attributes' => $component['extra']['attributes'],
    '#element_validate'  => array('_webform_validate_date_monthyear'),
    '#theme_wrappers' => array('webform_element_wrapper'),
    '#pre_render' => array('webform_element_title_display'),
    '#post_render' => array('webform_element_wrapper'),
    '#webform_component' => $component,
  );

  // Add an monthyear class for identifying the difference from normal textfields.
  $element['#attributes']['class'] = isset($element['#attributes']['class']) ? $element['#attributes']['class'] . ' monthyear' : 'monthyear';

  // Enforce uniqueness.
  if ($component['extra']['unique']) {
    $element['#element_validate'][] = 'webform_validate_unique';
  }

  if (isset($value)) {
    $element['#default_value'] = $value[0];
  }

  if ($component['extra']['disabled']) {
    $element['#attributes']['readonly'] = 'readonly';
  }

  // Change the 'width' option to the correct 'size' option.
  if ($component['extra']['width'] > 0) {
    $element['#size'] = $component['extra']['width'];
  }

  return $element;
}

/**
 * A Drupal Form API Validation function. Validates the entered values from
 * monthyear components on the client-side form.
 *
 * @param $form_element
 *   The monthyear form element.
 * @param $form_state
 *   The full form state for the webform.
 * @return
 *   None. Calls a form_set_error if the monthyear is not valid.
 */
function _webform_validate_date_monthyear($form_element, &$form_state) {
  $component = $form_element['#webform_component'];
  $value = trim($form_element['#value']);
  if (!empty($value) && !_webform_validate_monthyear($value)) {
    form_error($form_element, t('%value is not a valid response. Please enter a date between 01/1900 and 12/2099 in this format MM/YYYY.', array('%value' => $value)));
  }
  else {
    form_set_value($form_element, $value, $form_state);
  }
}

function _webform_validate_monthyear($date) {
	if (ereg("^(01|02|03|04|05|06|07|08|09|10|11|12)/(19|20)([0-9]{2})$", $date, $regs)) {
		return TRUE;
	}
	else {
		return FALSE;
	}
}

/**
 * Implementation of _webform_display_component().
 */
function _webform_display_date_monthyear($component, $value, $format = 'html') {
  return array(
    '#title' => $component['name'],
    '#weight' => $component['weight'],
    '#theme' => 'webform_display_date_monthyear',
    '#theme_wrappers' => $format == 'html' ? array('webform_element', 'webform_element_wrapper') : array('webform_element_text'),
    '#post_render' => array('webform_element_wrapper'),
    '#format' => $format,
    '#value' => isset($value[0]) ? $value[0] : '',
    '#webform_component' => $component,
  );
}

/**
 * Format the text output for this component.
 */
function theme_webform_display_date_monthyear($element) {
  $element['#value'] = empty($element['#value']) ? ' ' : $element['#value'];
  return $element['#format'] == 'html' ? check_plain($element['#value']) : $element['#value'];
}

/**
 * Implementation of _webform_analysis_component().
 */
function _webform_analysis_date_monthyear($component, $sids = array()) {
  $placeholders = count($sids) ? array_fill(0, count($sids), "'%s'") : array();
  $sidfilter = count($sids) ? " AND sid in (" . implode(",", $placeholders) . ")" : "";
  $query = 'SELECT data ' .
    ' FROM {webform_submitted_data} ' .
    ' WHERE nid = %d ' .
    ' AND cid = %d ' . $sidfilter;
  $nonblanks = 0;
  $submissions = 0;

  $result = db_query($query, array_merge(array($component['nid'], $component['cid']), $sids));
  while ($data = db_fetch_array($result)) {
    if (drupal_strlen(trim($data['data'])) > 0) {
      $nonblanks++;
    }
    $submissions++;
  }

  $rows[0] = array(t('Left Blank'), ($submissions - $nonblanks));
  $rows[1] = array(t('User entered value'), $nonblanks);
  //$rows[2] = array(t('Average submission length in words (ex blanks)'), ($nonblanks != 0 ? number_format($wordcount/$nonblanks, 2) : '0'));
  return $rows;
}

/**
 * Implementation of _webform_table_component().
 */
function _webform_table_date_monthyear($component, $value) {
  return check_plain(empty($value[0]) ? '' : $value[0]);
}


/**
 * Implementation of _webform_csv_headers_component().
 */
function _webform_csv_headers_date_monthyear($component, $export_options) {
  $header = array();
  $header[0] = '';
  $header[1] = '';
  $header[2] = $component['name'];
  return $header;
}

/**
 * Implementation of _webform_csv_data_component().
 */
function _webform_csv_data_date_monthyear($component, $export_options, $value) {
  return empty($value[0]) ? '' : $value[0];
}
